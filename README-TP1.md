# TP1 : Configuration système

# 0. Préliminaires
Installation de `nano` sur la machine afin de faciliter l'édition des fichiers.

```bash=
$ sudo yum install nano
```
Configuration de nano en tant qu'éditeur par défaut du système.

```bash=
$ cat <<EOF >>/etc/profile.d/nano.sh
$ export VISUAL="nano"
$ export EDITOR="nano"
$ EOF
```
# I. Utilisateurs

## 1. Création et configuration

Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :
* le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans `/home`
* le shell de l'utilisateur est `/bin/bash`

```bash= 
$ sudo adduser ouioui
$ sudo passwd ouioui
```

Créer un nouveau groupe `admins` qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.
Ajouter votre utilisateur à ce groupe `admins`.


```bash=
$ sudo groupadd admins
$ sudo nano /etc/sudoers
 «%admins ALL=(ALL)        ALL »
$ sudo usermod -a -G admins ouioui
```

# III. Partitionnement

## 1. Préparation de la VM

Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

## 2. Partitionnement

Utilisation de LVM :

Agréger les deux disques rajoutés à la VM en un seul *volume group*

```bash=
$ lsblk
« sdb 3GB»
« sdc 3GB»

sudo pvcreate /dev/sdb
sudo pvcreate /dev/sdc

sudo pvs
« format lvm2 »

$ sudo vgcreate data /dev/sdb /dev/sdc 



