# TP2 : Serveur Web
web.tp2.cesi 
192.168.141.130

db.tp2.cesi 
192.168.141.131

rp.tp2.cesi 
192.168.141.132

# I. Base de données

>  sur `db.tp2.cesi`.

* installer le paquet `mariadb-server`, puis démarrer le service associé :

```bash=
$ sudo yum install mariadb-server -y
$ sudo systemctl start mariadb.service
$ sudo mysql_secure_installation
```


* se connecter à la base
```bash=
$ sudo mysql -u root -p
Enter password:
```
```bash
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 12
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```
* créer une base
```bash=
MariaDB [(none)]> create database db_wordpress;
MariaDB [(none)]> Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> show databases;
MariaDB [(none)]> +--------------------+
    -> | Database           |
    -> +--------------------+
    -> | information_schema |
    -> | db_wordpress             |
    -> | mysql              |
    -> | performance_schema |
    -> +--------------------+
    -> 4 rows in set (0.00 sec)
```
* créer un utilisateur
```bash=
CREATE USER 'wordpress'@'web.tp2.cesi' IDENTIFIED BY 'wordpress';
GRANT ALL PRIVILEGES ON db_wordpress.* TO 'wordpress'@'192.168.141.130' 
  IDENTIFIED BY 'wordpress' WITH GRANT OPTION;
  FLUSH PRIVILEGES;
```
* ouvrir un port firewall pour que d'autres machines puissent accéder à la base
```bash=
$ sudo firewall-cmd --add-port=3306/tcp --permanent
success

$ sudo firewall-cmd --reload
```

